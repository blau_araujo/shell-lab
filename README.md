# Laboratório do Shell

Se você...

- Está com algum problema no seu script em shell;
- Não sabe que ferramentas e técnicas utilizar;
- Está com dúvidas sobre algo que encontrou [estudando pelo nosso material livre e gratuito](https://codeberg.org/blau_araujo/para-aprender-shell)...

Utilize as issues deste repositório para publicar seu problema e nós vamos resolver juntos para que toda a comunidade possa aprender. As dúvidas mais cabeludas, nós vamos resolver em um laboratório aberto pelo Jitsi. 

## Quanto custa

É de graça, mas você sempre pode utilizar uma das formas de apoio ao nosso trabalho:

* Doações via PIX: pix@blauaraujo.com
* [Doações pelo PicPay](https://app.picpay.com/user/blauaraujo)
* [Apoio mensal pelo Apoia.se](https://apoia.se/debxpcursos)

## Como publicar suas dúvidas

1. Para cada dúvida, clique na [aba issues](https://codeberg.org/blau_araujo/shell-lab/issues) e clique no botão **"Nova issue"**.
2. Escreva um título que tenha a ver com o problema e utilize a caixa de textos "Escrever" para detalhar seu problema.
3. Você pode escrever em markdown, especialmente se quiser publicar trechos de código ou saídas de comandos.
4. Não publique imagens/prints.
5. Clique no botão "Criar issue" e pronto!

É só aguardar as respostas!

## Importante!

- Tudo que for publicado neste repositório poderá ser utilizado para a criação de artigos e vídeos.
- A nossa licença é a [Creative Commons BY-SA 4.0 International](https://codeberg.org/blau_araujo/shell-lab/src/branch/main/LICENSE).
- Os laboratórios pelo Jitsi serão apenas para o que não puder ser resolvido por texto.
- Clique no botão "Observar" para acompanhar as mudanças neste repositório.
- Não há um tempo de resposta pré-determinado, seja paciente.
- Este é um trabalho voluntário para a popularização do shell do GNU/Linux.
- Eu serei notificado pelo Codeberg sobre todas as novas issues, mas você pode me citar nas postagens incluindo: @blau_araujo.




